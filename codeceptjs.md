![profil](./assets/profil.png)

[![Twitter URL](https://img.shields.io/twitter/url/https/twitter.com/cy_hue.svg?style=social&label=Follow%20%40cy_hue)](https://twitter.com/cy_hue)

@cy_hue

[https://horsty.fr](https://horsty.fr)

---

# Pourquoi j'ai migré mes E2E vers CodeceptJs ?

Note: #e2e #codeceptjs #playwright #automatisation #rex #kanoma
#typescript #test #front #javascript #CI/CC #Magictools #Gitlab #BDD #1/2

---

### Des E2E Flaky

> « Stop calling your tests flaky. Instead, call it ‘Random Success’ or ‘Sometimes Success.’ Would you want to release a product that ‘sometimes works’? »

> [Pavan Sudarshan, ThoughWorks](https://www.thoughtworks.com/insights/blog/no-more-flaky-tests-go-team)

Note:

- Expliquer context métier à la sg
- Debugger des tests flaky c’est compliqué

---

![Sample image](./assets/Misunderstanding.PNG)

Note:

- D'une part le po / business qui à des critéres d'acceptation
- De l'autre l'équipe de dev qui écrit du code
- Communication / compréhension compliqué

---
## Pyramide de test

![Sample image](./assets/pyramide-1.png)

Note:

- présenter la pyramide de test, montrer ou on se situe
- test couteux à maintenir vert
- si l'outil n'est pas bien, l'equipe aura des réticences à intervenir

---

### CodeceptJs c'est quoi ?

<img height=500 src="./assets/test-archi.svg">
<!-- ![architecture-codecept](./assets/test-archi.svg) -->

Note:

- Agnostique, surcouche
- Utilise des `moteur` d’automatisation
- Open source
- Scenario driven
- Debug interactif
- Des locators riches
- Redui la flakyness
- D'autre features / plugins
- utilise des `moteur` d’automatisation de parcours navigateur CodeceptJs les appels des `helpers`
- Codecepts c’est un framework de BDD (Behavior Driven Development) Comportement de l’utilisateur qui sert à implémenter le test - on écrit ce
  qu’un utilisateur pourrait décrire
- c’est open source
- « Scenario driven » , un PO peut rédiger des tests d’acceptante et ça peut presque être écrit tels quel dans le code
- Agnostique, codecept est une surcouche, en dessous on branche ce qu’on veut - facile de changer
- Interactive debug
- Les locators sont riches
- Reduire la flakyness
- et d’autre features
- break down communication barriers between business and technical teams

---

## Pourquoi j’aime utiliser CodeceptJs

---

## language comprehensif

```js
Scenario("Je peux postuler chez Kanoma", async ({ I }) => {
  I.amOnPage("https://www.kanoma.fr/join-the-tribe/");
  I.see("JOIN THE TRIBE !");
  I.fillField("Email", "cyril.hue@kanoma.fr");
  I.scrollTo(locate("#gform_submit_button_2"));
  I.see("Envoyer");
});
```

---

## le report allure

<img height=500 src="./assets/allure-report.png">

Note: Présenter les indicateurs d'allure

---

## Un debugging facile

Note: basculer en mode console pour montrer ce qu'on peut faire

---

## Les résultats ?

Note:

- Des devs heureux
- des tests faciles / rapide à débugger et corriger
- Optimiser les tests de 1h à 20min grace au locator / plugin
- custom testcafé qu'on à pu reproduire avec les customs helpers

---

## Les limites ?

- Test de sécurité OWASP
- Test de performance

---

![fin](./assets/gyzmo.jpeg)

---

### Un test end-to-end

- Plusieurs termes utilisés (bout en bout, test d'IHM, e2e, etc.)
- Peu fiable, souvent Flaky
- Difficile à maintenir
- Couteux
- Prend du temps
- Normalement, test la "tuyauterie", pas le métier

Note:

- Test de bout en bout ou test d’IHM, ce sont des tests qui sont lent,
- peu fiable, on parle de flakyness, couteux, et difficile à maintenir
- Tout comme les tests d’intégration, l’objectif d’un test de bout en bout est de valider la “tuyauterie”, aucunement le métier.

---

## les locator()

- Plusieurs stratégies (id, name, css, xpath, link, react, class or shadow)
- CGM - Codecept Guess Magic pour les locator
- Locate builder
- Strict locator

```js
// Locate builder
const elem = locate("a")
  .withAttr({ href: "#" })
  .inside(locate("label").withText("Hello"));
```

```js
// Strict locator
input[(type = password)];
{
  css: "input[type=password]";
}
```

Note:

- possibilité de crééer des customs locator, et des customs strict locator
- "fuzzy" locator. résolution par la découverte et l'apprentissage
- Maintaining locators must be calculated as part of the cost of test maintenance.

---

## Custom Helper

```js
const Helper = require("@codeceptjs/helper");
// extends class helper
class MyHelper extends Helper {
  _before() {}
  _after() {}
  doAwesomeThings() {
    const myHelper = this.helpers["helperName"];
    console.log("You can use those function :", myHelper);
  }
  doAwesomeThingsWithPlaywright() {
    const { Playwright } = this.helpers;
    Playwright.click("Awesome");
  }
}
module.exports = MyHelper;
```

```js
I.doAwesomeThings();
```

Note: Helper c'est pour unifier les `moteurs` avec le langage codecept

---

##### Plugins - autoLogin

```js
autoLogin: {
  enabled: true,
  saveToFile: false,
  inject: 'loginAs',
  users: {
    admin: {
      login: (I) => {
        // How i log in into the app
      },
      check: (I) => {
        // How i check the admin is log in
      },
      fetch: () => {}, // How i get the cookie
      restore: () => {}, // Where i save the cookie
```

```js
Before((loginAs) => {
  loginAs("admin"); // login using admin session
});
```

---

## Ressources

- [Best Practices | CodeceptJS](https://codecept.io/best/#focus-on-readability)
- [Writing reliable locators for Selenium and WebDriver tests – Firefox Test Engineering](https://blog.mozilla.org/fxtesteng/2013/09/26/writing-reliable-locators-for-selenium-and-webdriver-tests/)
- [We Are Switching From TestCafe to CodeceptJS – Here’s Why | platformOS](https://www.platformos.com/blog/post/we-are-switching-from-testcafe-to-codeceptjs-here-s-why)
- [La pyramide des tests par la pratique (1/5) | OCTO Talks !](https://blog.octo.com/la-pyramide-des-tests-par-la-pratique-1-5/)
